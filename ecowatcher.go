package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/exec"
	"time"
)

// ecowatcher will force a restart of EcoServer after this many failed attempts
// to connect to the watched port (2 second intervals)
const errorThreshold = 5

// ecowatcher will wait up to a minute (in 10 second intervals) before giving up trying
// to start EcoServer
const startupThreshold = 6

var ecoServerCommand *exec.Cmd
var killCmd *exec.Cmd
var cfg *WatcherConfig

// WatcherConfig provides port config to check Eco for uptime
type WatcherConfig struct {
	Port int `json:"port"`
}

func loadConfig() *WatcherConfig {
	data, err := ioutil.ReadFile("config.json")
	if err != nil {
		log.Println("Warning: config.json not found, starting up on default port 3000")
		return &WatcherConfig{
			Port: 3000,
		}
	}
	cfg := &WatcherConfig{}
	err = json.Unmarshal(data, cfg)
	if err != nil {
		log.Fatalf("Invalid config.json file")
	}
	return cfg
}

func restartEco() {
	log.Println("Killing Eco Server")
	killEco()
	killEco()
	startEco()
}

func killEco() {
	killCmd = exec.Command("taskkill", "/IM", "EcoServer.exe")
	killCmd.Stderr = os.Stderr
	killCmd.Stdout = os.Stdout
	err := killCmd.Run()
	if err != nil {
		log.Printf("Error killing eco: '%v'", err.Error())
	}
}

func startEco() {
	log.Println("Starting Eco Server")
	ecoServerCommand = exec.Command("cmd.exe", "/C", "start", "EcoServer")
	ecoServerCommand.Stderr = os.Stderr
	ecoServerCommand.Stdout = os.Stdout
	err := ecoServerCommand.Start()
	if err != nil {
		log.Fatalf("Error starting Eco Server: '%v'", err.Error())
	}
	// Wait up to one minute for server to start,
	// and give up if it fails.
	errors := 0
	for {
		time.Sleep(time.Second * 10)
		cnn, err := net.Dial("tcp", fmt.Sprintf("127.0.0.1:%v", cfg.Port))
		if err != nil {
			log.Printf("Waiting for server to accept connections...")
			errors++
			if errors >= startupThreshold {
				log.Fatalf("Unable to connect to Eco Server: '%v'", err.Error())
			}
		} else {
			cnn.Close()
			log.Println("Eco Server is now accepting connections")
			return
		}
	}
}

func main() {
	killCmd = exec.Command("taskkill", "/IM", "EcoServer.exe")
	killCmd.Stderr = os.Stderr
	killCmd.Stdout = os.Stdout

	cfg = loadConfig()
	log.Printf("Starting up Eco Server Watcher - watching port %v", cfg.Port)
	startEco()
	errors := 0
	for {
		time.Sleep(time.Second * 2)
		cnn, err := net.Dial("tcp", fmt.Sprintf("127.0.0.1:%v", cfg.Port))
		if err != nil {
			log.Printf("Error connecting to EcoServer: '%v'", err.Error())
			errors++
			if errors >= errorThreshold {
				restartEco()
			}
		} else {
			errors = 0
			cnn.Close()
		}
	}
}
