## Eco Watcher

Eco Watcher was created back in a time when the Eco multiplayer server was in its alpha days and unstable.
It will launch the server and check port 3000 (or a custom configured port) for connectivity, forcing a restart
if it detects that the server has become unresponsive.

### Building

If you already have a built version of ecowatcher.exe, you can skip to the "Running" step.
These instructions are for anyone who wants to build Eco Watcher from source.

First, make sure to install the Go sdk from here: https://golang.org/dl/

Navigate to the root of the project and run the following commands to build it (this only works on windows):
```
go get ./...
go build .
```

### Running

Copy ecowatcher.exe and config.json into the same folder as EcoServer.exe.
Double-clicking on ecowatcher.exe will launch EcoServer, and will begin monitoring
the server for connectivity.
